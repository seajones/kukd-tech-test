/* Utilities.js - functions used again and again */

var crypto = require('crypto');

module.exports = {};

module.exports.generatePWHash = function (salt, password) {

	// Generates a password hash for both creating users, and comparing user passwords

	return crypto.createHash('sha256').update(salt + password).digest('base64'); // Uses a simple hash function, a product app would likely use PBKDF2.

};