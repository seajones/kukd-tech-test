var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'techtest'
    },
    port: 3000,
    db: 'mysql://test:test@localhost/testdb'
  },

  test: {
    root: rootPath,
    app: {
      name: 'techtest'
    },
    port: 3000,
    db: 'mysql://test:test@localhost/testdb'
  },

  production: {
    root: rootPath,
    app: {
      name: 'techtest'
    },
    port: 3000,
    db: 'mysql://test:test@localhost/testdb'
  }
};

module.exports = config[env];
