

var express = require('express'),
  config = require('./config/config'),
  db = require('./app/models'),
  session = require('express-session'),
  MySQLStore = require('express-mysql-session')(session);

var app = express();

// Session
 
var options = {
  host: 'localhost',
  port: 3306,
  user: 'test',
  password: 'test',
  database: 'testdb'
};
 
var sessionStore = new MySQLStore(options);
 
app.use(session({
  key: 'kukdtechtest',
  secret: 'cc0e34c301574d6dd25577e5c49fa386', // MD5 of 'EASTER EGG', just FYI.
  store: sessionStore,
  resave: true,
  saveUninitialized: true
}));

require('./config/express')(app, config);

db.sequelize
  .sync()
  .then(function () {
    app.listen(config.port, function () {
      console.log('Express server listening on port ' + config.port);
    });
  }).catch(function (e) {
    throw new Error(e);
  });

