module.exports = function(sequelize, DataTypes) {

	var Restaurant = sequelize.define('Restaurant', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: DataTypes.STRING,
		postCode: DataTypes.STRING,
		cuisine: DataTypes.STRING
	});

	return Restaurant;
}