/* Auth model */

var crypto = require('crypto'),
	util = require('../../utilities.js');

module.exports = function(sequelize, DataTypes) {

	var User = sequelize.define('User', {
		fullName: DataTypes.STRING,
		email: DataTypes.STRING,
		blurb: DataTypes.STRING,
		salt: DataTypes.STRING,
		passwordHash: DataTypes.STRING
	});

	return User;

}