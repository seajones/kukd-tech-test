var express = require('express'),
  router = express.Router(),
  API = express.Router(),
  basket = express.Router(),
  db = require('../models'),
  session = require('express-session'),
  util = require('../../utilities'),
  _ = require('lodash'),
  paypal = require('paypal-rest-sdk');

// Paypal setup
paypal.configure({
	'mode': 'sandbox',
	'client_id': 'ATR7i-WKqO_7sY04qP0T6Xdk3IAhFbNzAFeqhvhOmednaXKAnb7hCRS3QYhp4xYUoMSGeP1Z-7IGUf_w',
	'client_secret': 'EAaNBFFGW4t_K5sNf8jfQZwS_cPFB8ml2jqKJxDHwXX2pYHNDyD6qLD-0Sju_6OnvguFhvs-dznXgaiJ'
});


module.exports = function (app) {
  app.use('/', router);
  app.use('/API', API);
  app.use('/basket', basket);
};

router.get('/', function (req, res, next) {
  // Home route!

  res.send('HOME');

});

// ******************
// * Account Routes *
// ******************

// Account home route
router.get('/account', function (req, res, next) {
	// Account Edit Page

	if (typeof req.session === 'undefined' || req.session.loggedIn != true) 
		res.redirect('/account/login');

	// Get user data
	db.User.findOne({
		where: {
			email: req.session.loggedInUser
		}
	}).then(function (User) {
		if (User === null) {
			// Fake User / expired session
			req.session.destroy
			res.redirect('/login');
			return;
		}

		var pageObj = {
			fullName: User.fullName,
			blurb: User.blurb,
			email: User.email
		};

		res.render('account', pageObj);
	});

});

// POST route for changes
router.post('/account', function (req, res, next) {
	if (typeof req.session === 'undefined' || req.session.loggedIn != true) 
		res.redirect('/account/login');
	else 
		next();
},function (req, res, next) {
	// Account edit submission

	var params = req.body,
		currentUser = req.session.loggedInUser;

	db.User.findOne({
		where: {
			email: currentUser
		}
	}).then(function (User) {
		if (User === 'null') {
			// Fake user session / expired session
			req.session.destroy();
			return res.redirect('/login');
		}

		if (typeof params.fullName !== 'undefined') User.fullName = params.fullName
		if (typeof params.email !== 'undefined') User.email = params.email;
		if (typeof params.blurb !== 'undefined') User.blurb = params.blurb;

		if (typeof params.pw1 !== 'undefined' || typeof params.pw2 !== 'undefined') {
			if (params.pw1 === params.pw2) {
				User.passwordHash = util.generatePWHash(User.salt, params.pw1);
			}
		}

		User
			.save()
			.then(function(user) {
			if (User === 'null') {
				// Fake user session / expired session
				req.session.destroy();
				return res.redirect('/login');
			}

			var pageObj = {
				fullName: User.fullName,
				blurb: User.blurb,
				email: User.email,
				message: 'User profile updated!'
			};

			res.render('account', pageObj);

		});

	});

});

// Login/logout redirects
router.get('/login', function (req, res, next) {
	res.redirect('/account/login');
});
router.get('/logout', function (req, res) {
	res.redirect('/account/logout');
});

// Actual Logout route
router.get('/account/logout', function (req, res) {
	req.session.destroy();
	res.render('logout');
	return;

});

// Account login page
router.get('/account/login', function (req, res, next) {
	// Account login page

	return res.render('login');

});

// Submit POST login
router.post('/account/login', function (req, res, next) {
	// Account Login submission

	var params = req.body,

		email = params.email,
		password = params.password;

	if (typeof email === 'undefined' || typeof password === 'undefined') {

		return res.redirect('/account/login');

	}

	db.User.findOne({
		where: {
			email: email
		}
	}).then(function (User) {

		if (User === null) {
			return res.redirect('/account/login');
		}

		var passwordHash = util.generatePWHash(User.salt + password);

		console.log(User.salt, password, passwordHash);

		if (passwordHash != User.passwordHash) {
			return res.redirect('/account/login');
		}

		req.session.loggedIn = true;
		req.session.loggedInUser = User.email;

		req.session.basket = [
			{id: 0,name: 'Item 1', description: 'Lorem Ipsum Dolor', price: 300},
			{id: 1,name: 'Item 2', description: 'Lorem Ipsum Dolor', price: 4000},
			{id: 2,name: 'Item 3', description: 'Lorem Ipsum Dolor', price: 299},
			{id: 3,name: 'Item 4', description: 'Lorem Ipsum Dolor', price: 100},
			{id: 4,name: 'Item 5', description: 'Lorem Ipsum Dolor', price: 1099},
			{id: 5,name: 'Item 6', description: 'Lorem Ipsum Dolor', price: 2399},
			{id: 6,name: 'Item 7', description: 'Lorem Ipsum Dolor', price: 599},
		];

		req.session.save();

		res.redirect('/account');

	});
});

// *****************
// * Basket Routes *
// *****************

// The basket main page

basket.get('/', function (req, res, next) {

	var basket = (typeof req.session.basket != 'undefined') ? req.session.basket : false;

	if (!basket || basket.length < 1) {

		return res.render('basket', {items: []});

	} else {

		var total = 0;
		basket.forEach(function (el) {total += el.price});

		return res.render('basket', {items: basket, total: total});

	}

});

// Basket Payment
basket.get('/pay', function (req, res, next) {

	// Validate basket
	var basket = (typeof req.session.basket != 'undefined') ? req.session.basket : false;

	if (!basket) return res.redirect('/basket');

	// Setup total charge
	var total = 0;

	basket.forEach(function (el) {total += el.price});

	total = (total / 100).toFixed(2).toString();

	// Start payment
	var payment = {
		"intent": "sale",
		"payer": {
			"payment_method": "paypal"
		},
		"redirect_urls": {
			"return_url": "http://localhost:3000/basket/execute",
			"cancel_url": "http://localhost:3000/basket/cancel"
		},
		"transactions": [{
			"amount": {
			"total": total,
			"currency": "GBP"
		},
			"description": "Kukd.com payment"
		}]
	};

	paypal.payment.create(payment, function (err, payment) {
		if (err) {
			console.log(err);
			res.redirect('/basket/error');
		} else {
			if(payment.payer.payment_method === 'paypal') {
				req.session.paymentId = payment.id;
				var redirectUrl;
				for(var i=0; i < payment.links.length; i++) {
					var link = payment.links[i];
					if (link.method === 'REDIRECT') {
						redirectUrl = link.href;
					}
				}
				  res.redirect(redirectUrl);
				}
		}
	});

});

// Basket payment handling

basket.get('/cancel', function (req, res, next) {
	res.render('basketfinish', {message: "Your payment was cancelled in PayPal."});
});

basket.get('/error', function (req, res, next) {
	res.render('basketfinish', {message: "An error occurred processing payment"});
});

basket.get('/execute', function (req, res, next) {

	var payment_id = (req.session.paymentId != '') ? req.session.paymentId : false,
		payerID = req.param('PayerID') || false,

		transactionDetails = {
			"payer_id" : payerID
		};

	if (!payment_id || !payerID) {
		return res.render('basketfinish', {message: "No payment has been successfully received"});
	}

	paypal.payment.execute(payment_id, transactionDetails, function (err, payment) {

		if (err) {
			console.log(err);
			return res.render('basketfinish', {message: "A PayPal error occurred"});
		} else {
			req.session.basket = [];
			req.session.save();
			return res.render('basketfinish', {message: "Thank you for your payment!"});
		}

	})

});

basket.get('/testitems', function (req, res, next) {
	req.session.basket = [
			{id: 0,name: 'Item 1', description: 'Lorem Ipsum Dolor', price: 300},
			{id: 1,name: 'Item 2', description: 'Lorem Ipsum Dolor', price: 4000},
			{id: 2,name: 'Item 3', description: 'Lorem Ipsum Dolor', price: 299},
			{id: 3,name: 'Item 4', description: 'Lorem Ipsum Dolor', price: 100},
			{id: 4,name: 'Item 5', description: 'Lorem Ipsum Dolor', price: 1099},
			{id: 5,name: 'Item 6', description: 'Lorem Ipsum Dolor', price: 2399},
			{id: 6,name: 'Item 7', description: 'Lorem Ipsum Dolor', price: 599},
		];
	req.session.save();
	res.redirect('/basket');
});

// Basket AJAX remove

basket.get('/remove/:id', function (req, res, next) {

	var basket = (typeof req.session.basket != 'undefined') ? req.session.basket : false;

	newBasket = [];

	if (_.isArray(basket)) {
		basket.forEach(function (el) {
			console.log(el.id, req.params.id);
			if (el.id !== parseInt(req.params.id)) {
				newBasket.push(el);
			}
		});
	}

	req.session.basket = newBasket;
	req.session.save();

	return res.redirect('/basket');

})


// ***********
// * The API *
// ***********
var APIKey = "a7cd39addb4c41f4c527f2f372629889";

API.get('/', function (req, res) {

	res.status(500).send('Incorrect use of API');

});

// C
API.post('/new', function (req, res) {

	var params = req.body;

	// check inputs
	if ((params.key == '') || (params.name == '') || (params.postCode == '') || (params.cuisine == '')) {
		return res.status(500).send('FAIL');

	}

	// check key
	if (params.key != APIKey) {
		return res.status(403).send('Not Authorised');
	}

	return res.send(JSON.stringify(db.Restaurant.create({
		name: params.name,
		postCode: params.postCode,
		cuisine: params.cuisine
	})));



});

// R - publicly accessible
API.get('/byType/:type', function (req, res) {
	db.Restaurant.findAll({
		where: {
			cuisine: req.params.type || null
		}
	}).then(function(Restaurants) {
		if (Restaurants === null) {
			res.status(404).send('Not Found.');
		} else {
			res.send(JSON.stringify(Restaurants));
		}
	});
});

API.get('/byPostCode/:postcode', function (req, res) {

	db.Restaurant.findAll({
		where: {
			postCode: {
				$like: req.params.postcode + '%'
			}
		}
	}).then(function(Restaurants) {
		if (Restaurants === null) {
			res.status(404).send('Not Found.');
		} else {
			res.send(JSON.stringify(Restaurants));
		}
	});
});

API.get('/byName/:name', function (req, res) {
	db.Restaurant.findOne({
		where: {
			name: req.params.name || null
		}
	}).then(function(Restaurant) {
		if (Restaurant === null) {
			res.status(404).send('Not Found.');
		} else {
			res.send(JSON.stringify(Restaurant));
		}
	})
});

API.get('/byId/:id', function (req, res) {
	db.Restaurant.findOne({
		where: {
			id: req.params.id || null
		}
	}).then(function(Restaurant) {
		if (Restaurant === null) {
			res.status(404);
		} else {
			res.send(JSON.stringify(Restaurant));
		}
	});
});

// U
API.post('/update', function (req, res) {
	var params = req.body;

	if (params.key == '' || params.key != APIKey) {
		return res.status(403).send('Not Authorised.')
	}

	if (params.id == '') {
		return res.status(500).send('Could not update');
	}

	db.Restaurant.findById(params.id)
		.then(function (Restaurant) {

			if (Restaurant === null) {
				return res.status(404).send('Not Found.');
			}

			var changeObj = {}

			if (typeof params.name != 'undefined') changeObj.name = params.name;
			if (typeof params.postCode != 'undefined') changeObj.postCode = params.postCode;
			if (typeof params.cuisine != 'undefined') changeObj.cuisine = params.cuisine;

			Restaurant.update(changeObj, {
				where: {id: params.id}
			}).then(function () {
				res.status(200).send('OK');
			}).error(function () {
				res.status(500).send('Could not update restaurant, even though it could find it.');
			});

		});
});

// D
API.delete('/delete/:id', function (req, res) {

	if (req.body.key != APIKey) {
		return res.send(403).send('Not Authorised');
	}

	db.Restaurant.findById(req.params.id).then(function (Restaurant) {
		Restaurant.destroy().then(function () {
			res.send('OK');
		});
	});

});